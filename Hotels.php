<?php

spl_autoload_register('autoloader');

function autoloader() {
	include_once 'pricesFilter.php';
	include_once 'destinationFilter.php';
	include_once 'hotelNameFilter.php';
	include_once 'dateFilteration.php';
	include_once 'HttpLayer.php';
}

use hotelfilters\pricesFilter;
use hotelfilters\dateFilteration;
use hotelfilters\destinationFilter;
use hotelfilters\hotelNameFilter;
use HttpLayer;



class searchHotels {
	// $price range examples  "500:600" or "100:300"
	// to sorting by hotel name value of $sort will be 'Name' and for sort by price $sort = 'Price'

	public function Search($pric_range = null , $destination =null , $hotelName = null , $date_range =null , $sort = null){

		$data = HttpLayer::getContent("https://api.myjson.com/bins/tl0bp");
		$data = HttpLayer::decodeResult($data , true);

		$beta_hotels = $data['hotels'];

		// filter results according to conditions
		if(!is_null($pric_range)){
			$priceFilterationClass = new pricesFilter();
			$prices = explode(":", $pric_range);
			$beta_hotels =  $priceFilterationClass->filter($prices[0] , $prices[1] , $beta_hotels);
		}

		if(!is_null($date_range)){
			$dateFilterationClass = new dateFilteration();
			$dates = explode(":", $date_range);
			$beta_hotels =  $dateFilterationClass->filter($dates[0] , $dates[1] , $beta_hotels);
		}

		if(!is_null($destination)){
			$destinationFilterClass = new destinationFilter();
			$beta_hotels =  $destinationFilterClass->filter($destination , $beta_hotels);

		}

		if(!is_null($hotelName)){
			$hotelNameFilterClass = new hotelNameFilter();
			$beta_hotels =  $hotelNameFilterClass->filter($hotelName , $beta_hotels);
		}

		// result sorting
		if(!empty($beta_hotels) && !is_null($sort)){

			if($sort == 'Name'){
				array_multisort(
					$beta_hotels->name,
					$beta_hotels->price, SORT_ASC, SORT_NUMERIC,
					$beta_hotels['city'],
					$beta_hotels['availability']
				);
			}elseif ($sort = 'Price'){
				$hotels_sorted_by_prices  = [];
				foreach ($beta_hotels as $one){
					$hotels_sorted_by_prices[$one['price']] = $one;
				}
				$beta_hotels = $hotels_sorted_by_prices;
				ksort($beta_hotels);
			}
		}

		if(!empty($beta_hotels))
			return $beta_hotels;
		else
			return 'no hotels match this critira ';
	}

}


$obj = new searchHotels();

// testing values
//		$pric_range  = "100:105";  '7-12-2020:13-12-2020'
//	    $date_range  = "8-10-2020:14-10-2020";
//		$destination = 'Par';
//	    $hotelName   = 'Media';

// filteration test
//	    $sort = 'Name';
//      $sort = 'Price';

print '<pre>';
print_r($obj->Search($pric_range = null , $destination = null , $hotelName = null , $date_range = null , $sort = null));