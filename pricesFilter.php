<?php
namespace hotelfilters;

/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 10/08/17
 * Time: 06:54 م
 */

include_once 'rangeFilteration.php';

use hotelfilters\rangeFilteration;


class pricesFilter implements rangeFilteration {

	public function filter( $from, $to, $hotels ) {
		$results =[];
		foreach ($hotels as $hotel){
			if($hotel['price'] >= $from && $hotel['price'] <= $to){
				$results [] = $hotel;
			}
		}

		return $results;
	}
}