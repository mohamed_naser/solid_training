<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/08/17
 * Time: 07:19 م
 */
namespace hotelfilters;

use hotelfilters\rangeFilteration;

class dateFilteration implements rangeFilteration {
	public function filter( $from, $to, $hotels ) {
		$results =[];
		$date_from = strtotime($from);
		$to_date   = strtotime($to);

		foreach ($hotels as $hotel){
			$available_dayes = $hotel['availability'];

			foreach ($available_dayes as $row){

				if(strtotime($row['from']) <= $date_from && strtotime ($row['to']) >= $to_date){
					$results [] = $hotel;
				}
			}
		}

		return $results;
	}
}