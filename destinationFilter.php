<?php

/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/08/17
 * Time: 07:22 م
 */
namespace hotelfilters;

include_once 'nameFilteration.php';
use hotelfilters\nameFilteration;

class destinationFilter implements nameFilteration {

	public function filter($destination, $hotels ) {
		$results = [];
		foreach ($hotels as $hotel ){
			if( is_numeric(stripos($hotel['city'] , $destination))){
				$results [] = $hotel;
			}
		}

		return $results;
	}
}