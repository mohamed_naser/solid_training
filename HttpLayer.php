<?php

/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/08/17
 * Time: 07:41 م
 */
class HttpLayer {

	public static function getContent($url){

		$data = file_get_contents($url);
		if(!empty($data))
			return $data;
		else
			return false;
	}

	public static function decodeResult ($result , $option = false){
		return json_decode($result , $option);
	}

	public static function encodeResult ($result){
		return json_encode($result);
	}
}