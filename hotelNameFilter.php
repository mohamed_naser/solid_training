<?php

/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/08/17
 * Time: 07:21 م
 */
namespace hotelfilters;

use hotelfilters\nameFilteration;

class hotelNameFilter implements nameFilteration {

	public function filter( $hotelName, $hotels ) {
		$results = [];

		foreach ($hotels as $hotel ){
			if( is_numeric(stripos($hotel['name'] , $hotelName))){
				$results [] = $hotel;
			}
		}

		return $results;
	}
}
